#include <iostream>
#include <string>
using namespace std;
int main() {
    int l1, l2; //for lengths of string
    char temp; //temporary char for swapping first char operator
    string a, b, c; //creating strings
    cin >> a;
    cin >> b;
    l1 = a.size(); //initialize size of string1
    l2 = b.size(); //initialize size of string2
    c = a + b; //summing strings
    temp = a[0];  //for
    a[0] = b[0];  // swapping  
    b[0] = temp;  //  operator
    cout << l1 << ' ' << l2 << endl; //prints lengths
    cout << c << endl << a << ' ' << b << endl; //prints strings
    return 0;
}