#include <sstream>
#include <vector>
#include <iostream>
using namespace std;
vector<int> parseInts(string str) { //vector function for parcelling
    vector<int>A; //vector for integers
    stringstream ss(str); //stringstream to parcel strings
    int x;
    char ch;
    while (true) { //while loop for parcelling
        ch = 'a'; //initialize the control variable to break the loop 
        ss >> x >> ch; //parcelling
        A.push_back(x); //taking integers to the vector
        if (ch != ',')break; //control condition, if string is over than break the loop
    }
    return A; //returns vector
}
int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str); //calling function
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n"; //prints integers in the vector
    }
    return 0;
}