#include <iostream>
using namespace std;
///Taking different variables from user by using scanf and print them to the screen.
int main() {
    int a;
    long b;
    char c;
    float d;
    double f;
    scanf("%d %ld %c %f %lf", &a, &b, &c, &d, &f);
    printf("%d\n%ld\n%c\n%f\n%lf\n", a, b, c, d, f);
    return 0;
}