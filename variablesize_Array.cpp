#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
int main() {
    int row, question, i, j, x, f, s, y; //variables
    cin >> row >> question; //taking first line 
    vector<vector<int>>list(row); //creating 2d vector
    for (i = 0; i < row; i++) {
        cin >> x; //taking k values
        for (j = 0; j < x; j++)
        {
            cin >> y; //taking numbers in a line
            list[i].push_back(y); //pushing themn to the vector
        }
    }
    for (int k = 0; k < question; k++) {
        cin >> f >> s; //taking row and col values for questions
        cout << list[f].at(s) << endl; //printing the answers
    }
    return 0;
}