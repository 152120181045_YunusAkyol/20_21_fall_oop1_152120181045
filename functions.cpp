#include <iostream>
#include <cstdio>
using namespace std;
int max_of_four(int a, int b, int c, int d) { //Finds the max number of four numbers.
    int max = a;
    if (max < b)
        max = b;
    if (max < c)
        max = c;
    if (max < d)
        max = d;
    return max;
}
int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d); //calling function
    printf("%d", ans);

    return 0;
}