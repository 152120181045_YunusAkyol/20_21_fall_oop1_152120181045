#include <iostream>
#include <sstream>
#include <string>
using namespace std;
/*!
 *  \brief     Student class.
 *  \details   This class is used to create student objects and keep their spesific properties.
 */
class Student {
private: //Creating variables
    int age, standard;
    string first_name, last_name;
public:
    void set_age(int a) { //Setting age
        age = a;
    }
    void set_first_name(string str) { //Setting first name
        first_name = str;
    }
    void set_last_name(string str) { //Setting last name
        last_name = str;
    }
    void set_standard(int a) { //Setting standard
        standard = a;
    }
    int get_age() { //Getting age from user
        return age;
    }
    string get_first_name() { //Getting first name from user
        return first_name;
    }
    string get_last_name() { //Getting last name from user
        return last_name;
    }
    int get_standard() { //Getting standard from user
        return standard;
    }
    string to_string() { //Convert the all values to the string
        stringstream ss;
        int a, b;
        a = age;
        b = standard;
        string x, y;
        ss << a;
        x = ss.str();
        ss.str("");
        ss.clear();
        ss << b;
        y = ss.str();
        string z = x + ',' + first_name + ',' + last_name + ',' + y;
        return z;
    }
};
int main() {
    int age, standard;                                       
    string first_name, last_name;
    cin >> age >> first_name >> last_name >> standard; //taking properties from user
    Student st; //creating object
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);
    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();
    return 0;
}