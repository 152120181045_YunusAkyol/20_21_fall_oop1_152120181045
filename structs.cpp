#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
struct Student { //creating struct for student id
    int age;
    string first_name;
    string last_name;
    int standard;
};
int main() {
    Student st; 
    cin >> st.age >> st.first_name >> st.last_name >> st.standard; //taking values
    cout << st.age << " " << st.first_name << " " << st.last_name << " " << st.standard; //printing values
    return 0;
}