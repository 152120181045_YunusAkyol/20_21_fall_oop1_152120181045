#include<iostream>
using namespace std;
/*!
 *  \brief     Box class.
 *  \details   This class is used to create boxes with their length,width and breadth quantities.
 */
class Box {
private:
	int l, b, h;//Variables
public:
	Box() { //Constructor
		l = 0;
		b = 0;
		h = 0;
	}
	Box(int length, int breadth, int height) { //To set the values
		l = length;
		b = breadth;
		h = height;
	}
	Box(const Box& B) { //Copy constructor
		l = B.l;
		b = B.b;
		h = B.h;
	}
	int getLength() { //Get length value from user.
		return l;
	}
	int getBreadth() { //Get width value from user.
		return b;
	}
	int getHeight() { //Get height value from user.
		return h;
	}
	long long CalculateVolume() { //Calculating the volume.
		long long vol;
		vol = (long long)l * b * h;
		return vol;
	}
	friend bool operator < (Box& A, Box& B) { //Returns bool function for the first condition.
		if ((A.l < B.l) || ((A.b < B.b) && (A.l == B.l)) || ((A.h < B.h) && (A.l == B.l) && (A.b == B.b))) {
			return true;
		}
		else {
			return false;
		}
	};
	friend ostream& operator<< (ostream& output, const Box& B) { // This function checks the second condition.
		output << B.l << " " << B.b << " " << B.h;
		return output;
	}
};

void check2()
{
	int n;
	cin >> n;
	Box temp;
	for (int i = 0; i < n; i++)
	{
		int type;
		cin >> type;
		if (type == 1)
		{
			cout << temp << endl;
		}
		if (type == 2)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			temp = NewBox;
			cout << temp << endl;
		}
		if (type == 3)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			if (NewBox < temp)
			{
				cout << "Lesser\n";
			}
			else
			{
				cout << "Greater\n";
			}
		}
		if (type == 4)
		{
			cout << temp.CalculateVolume() << endl;
		}
		if (type == 5)
		{
			Box NewBox(temp);
			cout << NewBox << endl;
		}

	}
}

int main()
{
	check2();
}