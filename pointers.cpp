#include <stdio.h>
void update(int* a, int* b) { //function for updating value of pointer
    int c = *a;
    (*a) += (*b);
    (*b) -= c;
    if (*b < 0) //non-negative condition
        *b = -(*b);
}
int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    scanf("%d %d", &a, &b);
    update(pa, pb); //calling function
    printf("%d\n%d", a, b);

    return 0;
}