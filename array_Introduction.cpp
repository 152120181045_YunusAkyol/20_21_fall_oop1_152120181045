#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
///Creating dynamic sized array and fill it in the first for loop.
///Print backwards in the second for loop.
int main() {
    int n;
    cin >> n;
    int *A;
    A = new int [n];
    for (int i = 0; i < n; i++) {
        cin >> A[i];
    }
    for (int i = n - 1; i >= 0; i--) {
        cout << A[i] << " ";
    }
    return 0;
}